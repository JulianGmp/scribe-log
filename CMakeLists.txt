cmake_minimum_required(VERSION 3.27)

project(
    scribe-log
    VERSION 0.1.0
    LANGUAGES CXX
)

add_library(${PROJECT_NAME} INTERFACE)
target_include_directories(${PROJECT_NAME}
    INTERFACE ${PROJECT_SOURCE_DIR}/inc
)
# require at least C++20, sadly there seems to not be a compile feature for the
# standard formatting library, which we could use instead
target_compile_features(${PROJECT_NAME} INTERFACE
    cxx_std_20
)

# we only add the example target when this is the root project
if(CMAKE_SOURCE_DIR STREQUAL CMAKE_CURRENT_SOURCE_DIR)
    message("Adding scribe-log example target")
    add_subdirectory(example)
endif()
