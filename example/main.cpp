#include "slog.hpp"

#include <cstdlib>

void basic_example()
{
    slog::debug("I'm a debug message");
    slog::info("I'm informative, look at this number: {}", 42);
    slog::warn("Careful, {} > {} is actually {}!", 10, 9, 10 > 9);
    slog::error("Something went wrong, but I'm not sure what");
}

void runtime_determined_loglevel_example()
{
    srand(time(nullptr));
    const uint32_t random_number = rand() % 100;

    const auto log_level = (random_number % 2 == 0) ? slog::LogLevel::Warn : slog::LogLevel::Info;
    // you must also set the location and timestamp, most of the time you want to use now and current
    slog::log_message_runtime(slog::logger_clock::now(),
                              std::source_location::current(),
                              log_level,
                              "a random int: {} (0x{:08x})",
                              random_number,
                              random_number);
}

void overwrite_location_and_time_example(const std::source_location& location)
{
    const auto actual_location = std::source_location::current();
    slog::log_message<slog::LogLevel::Info>(slog::logger_clock::now() + std::chrono::hours(1),
                                            location,
                                            "I'm {} from the future!",
                                            actual_location.function_name());
}

void runtime_log_level_example()
{
    slog::warn("Here comes a warning");
    slog::runtime_log_level() = slog::LogLevel::Error;
    slog::warn("Is anyone listening?");
    slog::error("Maybe, the log level is {}", slog::runtime_log_level().load());
    slog::runtime_log_level() = slog::LogLevel::Debug;
    slog::debug("Hey I'm back!");
}

int main()
{
    std::cout << "################## basic_example" << std::endl;
    basic_example();
    std::cout << std::endl << std::endl;

    std::cout << "################## runtime_determined_loglevel_example" << std::endl;
    runtime_determined_loglevel_example();
    std::cout << std::endl << std::endl;

    std::cout << "################## overwrite_location_and_time_example" << std::endl;
    overwrite_location_and_time_example(std::source_location::current());
    std::cout << std::endl << std::endl;

    std::cout << "################## runtime_log_level_example" << std::endl;
    runtime_log_level_example();
    std::cout << std::endl << std::endl;

    return 0;
}
