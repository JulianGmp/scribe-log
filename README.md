# scribe-log

A (somewhat opinionated) header only logging library utilizing C++20's `std::source_location` and
`std::format` that simply writes to standard out and not much else.  
It's quite stupid and just slightly over engineered.

Very much inspired by [Rust's log crate](https://github.com/rust-lang/log), but not designed to be
as flexible.

## Basic Usage

Simply include the header and use one of the four "functions" to log a message at its corresponding
log level.

```cpp
#include "slog.hpp"

void basic_example()
{
    slog::debug("I'm a debug message");
    slog::info("I'm informative, look at this number: {}", 42);
    slog::warn("Careful, {} > {} is actually {}!", 10, 9, 10 > 9);
    slog::error("Something went wrong, but I'm not sure what");
}
```
outputs:
```
[2024-06-18 20:19:07] [main.cpp:7] [Debug] I'm a debug message
[2024-06-18 20:19:07] [main.cpp:8] [Info] I'm informative, look at this number: 42
[2024-06-18 20:19:07] [main.cpp:9] [Warn] Careful, 10 > 9 is actually true!
[2024-06-18 20:19:07] [main.cpp:10] [Error] Something went wrong, but I'm not sure what
```

### Runtime log level

The log level is controlled via a globally accessible atomic variable.
```cpp
void runtime_log_level_example()
{
    slog::warn("Here comes a warning");
    slog::runtime_log_level() = slog::LogLevel::Error;
    slog::warn("Is anyone listening?");
    slog::error("Maybe, the log level is {}", slog::runtime_log_level().load());
    slog::runtime_log_level() = slog::LogLevel::Debug;
    slog::debug("Hey I'm back!");
}
```
outputs:
```
[2024-06-18 20:22:38] [main.cpp:39] [Warn] Here comes a warning
[2024-06-18 20:22:38] [main.cpp:42] [Error] Maybe, the log level is Error
[2024-06-18 20:22:38] [main.cpp:44] [Debug] Hey I'm back!
```

Note that `slog` reads the runtime log level using `std::memory_order_acquire`. When setting the
runtime log level, use the appropriate memory order for your application.
When in doubt, use `std::memory_order_seq_cst` (aka just assign it with `=`).

## Advanced Usage

### Using a log level determined at runtime

```cpp
void runtime_determined_loglevel_example()
{
    srand(time(nullptr));
    const uint32_t random_number = rand() % 100;

    const auto log_level = (random_number % 2 == 0) ? slog::LogLevel::Warn : slog::LogLevel::Info;
    // you must also set the location and timestamp, most of the time you want to use now and current
    slog::log_message_runtime(slog::logger_clock::now(),
                              std::source_location::current(),
                              log_level,
                              "a random int: {} (0x{:08x})",
                              random_number,
                              random_number);
}
```
outputs
```
[2024-06-18 20:22:38] [main.cpp:21] [Info] a random int: 65 (0x00000041)
```
or
```
[2024-06-18 20:24:08] [main.cpp:21] [Warn] a random int: 90 (0x0000005a)
```

### Overwriting the source location or timestamp
```cpp
void overwrite_location_and_time_example(const std::source_location& location)
{
    const auto actual_location = std::source_location::current();
    slog::log_message<slog::LogLevel::Info>(slog::logger_clock::now() + std::chrono::hours(1),
                                            location,
                                            "I'm {} from the future!",
                                            actual_location.function_name());
}
```
outputs:
```
[2024-06-18 21:24:08] [main.cpp:58] [Info] I'm void overwrite_location_and_time_example(const std::source_location&) from the future!
```

### Setting the compile time log level

Inside the `slog.hpp` header is a compile time log level (`slog::COMPILE_TIME_LOG_LEVEL`), which by
default is set to `Debug`, thus all messages are logged.  
You may overwrite this by setting the `SCRIBE_LOG_COMPILE_TIME_LOG_LEVEL` macro to any one of the
`slog::LogLevel` enum values, ideally this should be done by your build system to ensure it's the
same in every translation unit.

And yes, macro are ugly and horrible and I avoid them wherever I can, but I don't think you can
avoid them in this case. Note that is the *only* macro in the project.
The compile time log level is made available as the constexpr value `slog::COMPILE_TIME_LOG_LEVEL`.

The logging "functions" `debug`, `info`, `warn` and `error` check this log level *at compile time*,
using a `if constexpr`, so there should not be any overhead, at least in most release builds.

The `log_message_runtime` "function" checks this *at runtime* for obvious reasons.

Note that a message has to be greater than or equal to the compile time log level *and* the runtime
log level in order to be written.

### Examples

All of the examples can be found in `examples/main.cpp`. The provided cmake file allows you to test
around with it:
```
cmake -S . -B build
cmake --build build
./build/scribe-log_example
```

## Caveats

The actual implementation (`slog::internals::log_message_impl`) uses `std::osyncstream(std::cout)`
to write to standard out. This means
* this is probably not as efficient as it could be
* any access to cout without the `std::osyncstream` wrapper may cause messages to be interleaved in
  a multi threaded scenario

I only tested this on Linux systems using the GCC, I have no idea if it'll work on other platforms
or with other compilers.

Also, in order to allow the log "functions" to take the source location automatically while also
giving them an arbitrary number of template arguments for `std::format`, you have to use a user
defined template deduction guide, but these are only available for template types, not functions.  
As a result, `debug`, `info`, `warn` and `error` *aren't actually functions, but structs!* "Calling"
them is effectively creating a temporary and calling its constructor. It's a bit of a mess but it
makes the usage so much nicer.  
If you have a suggestion on how to do this in a more elegant way, feel free to reach out or open a
pull request.

## Additional Features

* Can I configure this to do *thing*?
* Can I log somewhere other than cout?
* Can you add *feature*?

To answer all of them quickly: nah  
If you want to customize this, change the header. It's a bit convoluted but < 300 lines.

I intend this to be as simple as I can while still giving me the nice to use syntax of
`std::format` and `std::source_location`.  
If you look for something more flexible take a look at [spdlog](https://github.com/gabime/spdlog),
it's fairly similar to use but covers a lot more functionality.

# License

Licensed under the MIT License, see [LICENSE](LICENSE).  
