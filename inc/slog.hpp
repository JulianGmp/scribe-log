#pragma once

#include <atomic>
#include <chrono>
#include <cstdint>
#include <format>
#include <iostream>
#include <optional>
#include <ranges>
#include <source_location>
#include <stdexcept>
#include <syncstream>

namespace slog
{

/** The log levels that can be used with scribe-log. */
enum class LogLevel : std::int32_t
{
    Debug = -1,
    Info = 0,
    Warn = 1,
    Error = 2
};

#ifdef SCRIBE_LOG_COMPILE_TIME_LOG_LEVEL
/**
 * The log level at compile time, set by the SCRIBE_LOG_COMPILE_TIME_LOG_LEVEL macro.
 * No message below this level will actually be logged, even when using log_message_runtime.
 */
constexpr LogLevel COMPILE_TIME_LOG_LEVEL = LogLevel::SCRIBE_LOG_COMPILE_TIME_LOG_LEVEL;
#else
/**
 * The log level at compile time, defaulted to Debug (i.e. all messages).
 * No message below this level will actually be logged, even when using log_message_runtime.
 */
constexpr LogLevel COMPILE_TIME_LOG_LEVEL = LogLevel::Debug;
#endif

/** The (global) log level checked at runtime. */
[[nodiscard]] inline std::atomic<LogLevel>& runtime_log_level()
{
    // this needs to be a function in order for the variable to be truly global and not cloned for each translation
    // unit. This would be easier if this was a C++20 module but at the time of writing these are not well supported,
    // let alone widely used.
    static std::atomic<LogLevel> actual_value = LogLevel::Debug;
    return actual_value;
}

/** The std::chrono clock type used to generate time stamps for the log messages */
using logger_clock = std::chrono::system_clock;

/** Get a string_view of the given LogLevel value */
[[nodiscard]] constexpr std::string_view log_level_to_str(LogLevel log_level)
{
    switch (log_level)
    {
    case LogLevel::Debug:
        return "Debug";
    case LogLevel::Info:
        return "Info";
    case LogLevel::Warn:
        return "Warn";
    case LogLevel::Error:
        return "Error";
    }
    // Exceptions are ugly but I definetly do not want to just return "UNKNOWN" or something like that
    throw std::logic_error(std::format("{} out of range!", __PRETTY_FUNCTION__));
}

/**
 * Try to parse a string_view into a LogLevel, matches against the actual value names, their upper and lower case
 * variants (e.g. "Debug", "debug" and "DEBUG").
 */
[[nodiscard]] constexpr std::optional<LogLevel> log_level_from_str(std::string_view str)
{
    if (str == "Debug" || str == "debug" || str == "DEBUG")
    {
        return LogLevel::Debug;
    }
    if (str == "Info" || str == "info" || str == "INFO")
    {
        return LogLevel::Info;
    }
    if (str == "Warn" || str == "warn" || str == "WARN")
    {
        return LogLevel::Warn;
    }
    if (str == "Error" || str == "error" || str == "ERROR")
    {
        return LogLevel::Error;
    }
    return std::nullopt;
}

namespace internals
{

[[nodiscard]] constexpr std::string_view get_filename_from_path(std::string_view path)
{
    // yes this won't work for windows paths, no I don't care.
    constexpr char delimeter = '/';

    const auto last_delim_index = path.find_last_of(delimeter);
    if (last_delim_index == path.npos || last_delim_index >= (path.size() - 2))
    {
        return path;
    }
    return path.substr(last_delim_index + 1);
}

template <class... Args>
void log_message_impl(logger_clock::time_point timestamp,
                      LogLevel log_level,
                      const std::source_location& location,
                      std::format_string<Args...> fmt,
                      Args&&... args)
{
    if (log_level < runtime_log_level().load(std::memory_order_acquire))
    {
        return;
    }

    // This is where we would use std::print if it wasn't C++23 specific
    // Yes, we have to call format twice here, as far as I know you cannot easily append two format strings without
    // avoiding a std::string allocation anyway.
    // The reason we pass one format call to the other is so that we only call the << operator on cout once. This has
    // nice effect that, since operator<< is thread safe on cout, log messages don't get intermixed with each other when
    // calling log functions from different threads.
    std::cout << std::format("[{:%F %H:%M:%OS}] [{}:{}] [{}] {}\n",
                             timestamp,
                             get_filename_from_path(std::string_view(location.file_name())),
                             location.line(),
                             log_level,
                             std::format(std::move(fmt), std::forward<Args>(args)...));
}

} // namespace internals

/**
 * Log a formatted message with the given timestamp and source location.
 * This takes the log level as a template parameter to efficiently check it at compile time.
 * @param timestamp The time for this message
 * @param location the source location for this message
 * @param fmt The format string for std::format
 * @param args The arguments to be formatted in std::format
 */
template <LogLevel log_level, class... Args>
void log_message(logger_clock::time_point timestamp,
                 const std::source_location& location,
                 std::format_string<Args...> fmt,
                 Args&&... args)
{
    if constexpr (log_level >= COMPILE_TIME_LOG_LEVEL)
    {
        internals::log_message_impl(timestamp, log_level, location, std::move(fmt), std::forward<Args>(args)...);
    }
}

/**
 * Log a formatted message with the given log level and overwrite the source location and timestamp.
 * @param timestamp The time for this message
 * @param location the source location for this message
 * @param log_level the log level for this message
 * @param fmt The format string for std::format
 * @param args The arguments to be formatted in std::format
 */
template <class... Args>
void log_message_runtime(logger_clock::time_point timestamp,
                         const std::source_location& location,
                         LogLevel log_level,
                         std::format_string<Args...> fmt,
                         Args&&... args)
{
    // we check against the compile time log level, even if a messages log level is given at
    // runtime to make sure that no message is logged when it is below the compile time level.
    // In some cases the compiler may optimize this away, when the log level is actually known at
    // compile time, but no guarantees.
    if (log_level >= COMPILE_TIME_LOG_LEVEL)
    {
        internals::log_message_impl(timestamp, log_level, location, std::move(fmt), std::forward<Args>(args)...);
    }
}

// You might wonder why these simple to use log functions look like this (and are actually structs
// instead of functions)...
// The answer: when we want to have both a parameter pack for std::format and the source location being defaulted to
// current, we have to use a user-defined deduction guide, to explicitly tell the compiler that a call like
// `debug("cool {} format", some_variable)` will be deduced to
// `debug(fmt, some_variable, location = std::source_location::current()).`
// Otherwise, the compiler may assume that the last argument (some_variable) should be the source_location. And at the
// time of writing, this is only supported for template types, not template functions.
// see https://stackoverflow.com/a/57548488
// Note that this means you can't actually pass in a source_location as the last argument, in order
// to manually set the location you have to use the other "function" (constructor)

template <class... Args>
struct debug
{
    /**
     * Log a formatted message with LogLevel::Debug at the current source location.
     * @param fmt The format string for std::format
     * @param args The arguments to be formatted in std::format
     * @param location Implicitly defaulted source location. You can't actually set this, because of the parameter pack,
     *                 if you want to overwrite the location use log_message instead.
     */
    debug(std::format_string<Args...> fmt,
          Args&&... args,
          std::source_location location = std::source_location::current())
    {
        log_message<LogLevel::Debug, Args...>(
            logger_clock::now(), location, std::move(fmt), std::forward<Args>(args)...);
    }
};
// template deduction guides to debug
template <class... Args>
debug(std::format_string<Args...>, Args&&...) -> debug<Args...>;

template <class... Args>
struct info
{
    /**
     * Log a formatted message with LogLevel::Info at the current source location.
     * @param fmt The format string for std::format
     * @param args The arguments to be formatted in std::format
     * @param location Implicitly defaulted source location. You can't actually set this, because of the parameter pack,
     *                 if you want to overwrite the location use log_message instead.
     */
    info(std::format_string<Args...> fmt,
         Args&&... args,
         std::source_location location = std::source_location::current())
    {
        log_message<LogLevel::Info, Args...>(
            logger_clock::now(), location, std::move(fmt), std::forward<Args>(args)...);
    }
};
// template deduction guides to info
template <class... Args>
info(std::format_string<Args...>, Args&&...) -> info<Args...>;

template <class... Args>
struct warn
{
    /**
     * Log a formatted message with LogLevel::Warn at the current source location.
     * @param fmt The format string for std::format
     * @param args The arguments to be formatted in std::format
     * @param location Implicitly defaulted source location. You can't actually set this, because of the parameter pack,
     *                 if you want to overwrite the location use log_message instead.
     */
    warn(std::format_string<Args...> fmt,
         Args&&... args,
         std::source_location location = std::source_location::current())
    {
        log_message<LogLevel::Warn, Args...>(
            logger_clock::now(), location, std::move(fmt), std::forward<Args>(args)...);
    }
};
// template deduction guides to warn
template <class... Args>
warn(std::format_string<Args...>, Args&&...) -> warn<Args...>;

template <class... Args>
struct error
{
    /**
     * Log a formatted message with LogLevel::Error at the current source location.
     * @param fmt The format string for std::format
     * @param args The arguments to be formatted in std::format
     * @param location Implicitly defaulted source location. You can't actually set this, because of the parameter pack,
     *                 if you want to overwrite the location use log_message instead.
     */
    error(std::format_string<Args...> fmt,
          Args&&... args,
          std::source_location location = std::source_location::current())
    {
        log_message<LogLevel::Error, Args...>(
            logger_clock::now(), location, std::move(fmt), std::forward<Args>(args)...);
    }
};
// template deduction guides to error
template <class... Args>
error(std::format_string<Args...>, Args&&...) -> error<Args...>;

} // namespace slog

// the formatter library is odd because this has to be in the std namespace which feels wrong
/** std::formatter specilization for slog::LogLevel, simply calls slog::log_level_to_str. */
template <>
struct std::formatter<slog::LogLevel>
{
    constexpr auto parse(std::format_parse_context& ctx)
    {
        return ctx.begin();
    }

    auto format(slog::LogLevel log_level, std::format_context& ctx) const
    {
        return format_to(ctx.out(), "{}", slog::log_level_to_str(log_level));
    }
};
